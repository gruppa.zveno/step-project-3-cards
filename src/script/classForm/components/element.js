class Component {
  // here we create a constructor and describe rendering on a page and return an element
  constructor(props) {
    this.props = { ...props };
  }

  createElement(tag, attr, content = "") {
    const element = document.createElement(tag);
    for (const [key, value] of Object.entries(attr)) {
      if (value) {
        element.setAttribute(key, value);
      }
    }
    if (content) {
      element.innerHTML = content;
    }
    return element;
  }
}

class Element extends Component {
  // adding "this" element, inheritance and class methods (what exactly gets into rendering)
  render() {
    const { tag, content, handler, ...attr } = this.props;

    const element = this.createElement(tag, attr, content);
    this.element = element;
    return element;
  }
  handler() {
    const { handler } = this.props;
    this.element.addEventListener(`${handler.event}`, handler.func);
  }
}

export { Component };
export default Element;
