export { default as Input } from "./components/input";
export { default as Element } from "./components/element";
export { default as LoginForm } from "./loginForm";
export { default as RegisterForm } from "./registerForm";

export { default as CardioForm } from "./cardioForm";
export { default as DentistForm } from "./dentistForm";
export { default as TherapistForm } from "./therapistForm";
