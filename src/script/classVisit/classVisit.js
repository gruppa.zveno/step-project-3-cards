export { default as VisitDentist } from "./visitDentist";
export { default as VisitTherapist } from "./visitTherapist";
export { default as VisitCardiologist } from "./visitCardio";
