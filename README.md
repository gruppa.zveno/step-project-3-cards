Developers:

1. [Elijah Romashchenko](https://gitlab.com/romule)
2. [Dmitriy Antoschenko](https://gitlab.com/dantonmail)
3. [Vladyslav Katerusha](https://gitlab.com/gruppa.zveno)
4. [Dmitriy Blyakharskiy](https://gitlab.com/wargnom.dimon)
5. [Natalia Tolubarya](https://gitlab.com/tolubarka)

[Figma layout](<https://www.figma.com/file/hIZBqSAczY88psITTsCnjI/Step%233---cards-(to-do-list)---VISITER?node-id=0%3A1>)

[Trello dash-board](https://trello.com/b/k1PMfsDD)

Token: 712dded4-7450-488a-8058-538bc0777cbb
